package pro.javatar.pipeline.simple.service.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleJavaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleJavaServiceApplication.class, args);
	}

}
