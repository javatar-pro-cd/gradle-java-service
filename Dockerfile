FROM openjdk:13.0.1-slim

WORKDIR /service

ENV CONTAINER_OPTS "-XX:+UseContainerSupport -XX:MaxRAMPercentage=80"
ENV JAVA_OPTS ""
ENV SERVICE_PARAMS ""
ENV SERVICE_ADDITIONAL_PARAMS ""

ADD build/libs/gradle-java-service.jar /service/
CMD java $CONTAINER_OPTS $JAVA_OPTS -jar gradle-java-service.jar $SERVICE_PARAMS $SERVICE_ADDITIONAL_PARAMS
